resource "aws_iam_user" "user" {
  name = var.site
  path = "/"
}

resource "aws_iam_user_policy" "s3_policy" {
  name = "AccessUserSiteBucket"
  user = aws_iam_user.user.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::*.${var.site}.com",
        "arn:aws:s3:::*.${var.site}.com/*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_access_key" "access_key" {
  user    = aws_iam_user.user.name
  pgp_key = var.pgp_public_key
}

