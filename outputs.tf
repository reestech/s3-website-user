output "user" {
  value = aws_iam_user.user.name
}

output "key" {
  value = aws_iam_access_key.access_key.id
}

output "secret" {
  value = aws_iam_access_key.access_key.encrypted_secret
}

